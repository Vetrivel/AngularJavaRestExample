package com.avaldes.processor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.avaldes.dbconnection.DbConnection;
import com.avaldes.model.Actor;

public class ActorProcessor {

	private Connection connection;

	public ActorProcessor() {

		connection = DbConnection.getConnection();

	}

	public Actor addactor(Actor actor) {

		try {

			PreparedStatement preparedStatement = connection

					.prepareStatement("insert into task(id,actor_name,actor_birthname,actor_email,actor_image,actor_isactive,actor_birthdate) values (?, ?, ?,?,?,?,?)");

			preparedStatement.setInt(1, actor.getId());

			preparedStatement.setString(2, actor.getName());

			preparedStatement.setString(3, actor.getBirthName());

			preparedStatement.setString(4, actor.getEmail());

			preparedStatement.setString(5, actor.getImage());
			preparedStatement.setBoolean(6, actor.isActive());
			preparedStatement.setString(7, actor.getBirthDate());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		}
		return actor;
	}

	public ArrayList<Actor> searchactor() {
		ArrayList<Actor> actorsList = new ArrayList<Actor>();
		try {

			Statement stmt = connection.createStatement();
			String query = "SELECT * FROM task";
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {

				Actor actor = new Actor();

				actor.setId(rs.getInt("id"));
				actor.setName(rs.getString("actor_name"));
				actor.setBirthName(rs.getString("actor_birthname"));
				actor.setEmail(rs.getString("actor_email"));
				actor.setImage(rs.getString("actor_image"));
				// actor.setActive(false);
				actor.setBirthDate(rs.getString("actor_birthdate"));
				actorsList.add(actor);

			}

		} catch (SQLException e) {

			e.printStackTrace();

		}
		return actorsList;

	}

	public Actor getActor(int id) {
		Actor actor = new Actor();
		try {

			PreparedStatement preparedStatement = connection
					.prepareStatement("select * from task where id=?");
			preparedStatement.setInt(1, id);
			ResultSet rs = preparedStatement.executeQuery();

			if (rs.next()) {

				actor.setId(rs.getInt("id"));
				actor.setName(rs.getString("actor_name"));
				actor.setBirthName(rs.getString("actor_birthname"));
				actor.setEmail(rs.getString("actor_email"));
				actor.setImage(rs.getString("actor_image"));
				// actor.setActive(false);
				actor.setBirthDate(rs.getString("actor_birthdate"));
			}

		} catch (SQLException e) {

			e.printStackTrace();

		}
		return actor;

	}

	public Actor updateActor(int id, Actor a) {

		try {

			PreparedStatement preparedStatement = connection
					.prepareStatement("update task set actor_name=?,actor_birthname=?,actor_email=?,actor_image=?,actor_isactive=?,actor_birthdate=? where id="
							+ id);

			preparedStatement.setString(1, a.getName());
			preparedStatement.setString(2, a.getBirthName());
			preparedStatement.setString(3, a.getEmail());
			preparedStatement.setString(4, a.getImage());
			preparedStatement.setBoolean(5, a.isActive());
			preparedStatement.setString(6, a.getBirthDate());

			// preparedStatement.setInt(8, id);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();

		}
		return a;

	}

	public void deleteActor(int id) {

		try {

			PreparedStatement preparedStatement = connection
					.prepareStatement("delete from task where id=" + id);

			preparedStatement.executeUpdate();
		} catch (SQLException e) {

			e.printStackTrace();

		}

	}

}
