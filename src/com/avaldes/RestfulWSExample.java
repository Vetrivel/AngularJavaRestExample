package com.avaldes;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.avaldes.model.Actor;
import com.avaldes.processor.ActorProcessor;

@Path("/actors")
public class RestfulWSExample {
	static final String api_version = "1.01A rev.18729";
	static Logger logger = Logger.getLogger(RestfulWSExample.class);
	static String xmlString = null;

	@Path("/version")
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String returnVersion() {
		return "<p>Version: " + api_version + "</p>";
	}

	// This is the default @PATH
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ArrayList<Actor> getAllActors() {
		System.out.println("Getting all actors...");
		ActorProcessor ap = new ActorProcessor();
		return ap.searchactor();

	}

	@Path("{id}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Actor getActorById(@PathParam("id") int id) {
		System.out.println("Getting actor by ID: " + id);
		ActorProcessor ap = new ActorProcessor();
		return ap.getActor(id);
	}

	@Path("{id}")
	@PUT
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Actor updateActor(@PathParam("id") int id, Actor actor) {

		ActorProcessor ap = new ActorProcessor();
		return ap.updateActor(id, actor);
	}

	@Path("/search/{query}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ArrayList<Actor> searchActorByName(@PathParam("query") String query) {
		System.out.println("Searching actor by Name: " + query);
		ActorProcessor ap = new ActorProcessor();
		ArrayList<Actor> actors = ap.searchactor();
		System.out.println("lllllllllll"+actors.size());
		
		ArrayList<Actor> actorList = new ArrayList<Actor>();
		for (Actor c : actors) {
			if (c.getName().toUpperCase().contains(query.toUpperCase()))
				actorList.add(c);
		}
		return actorList;
	}

	@Path("/add")
	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Actor addActor(Actor actor) {
		System.out.println("Adding actor with ID: " + actor.getId());
		System.out.println("Inside addActor, returned: " + actor.toString());
		ActorProcessor ap = new ActorProcessor();
		return ap.addactor(actor);
	}

	@Path("{id}")
	@DELETE
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void deleteActorById(@PathParam("id") int id) {
		System.out.println("Deleting actor with ID: " + id);
		ActorProcessor ap = new ActorProcessor();
		ap.deleteActor(id);
	}
}
